import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html'
})
export class FormComponent implements OnInit {

  private cliente: Cliente = new Cliente()
  private titulo: string

  constructor(
    private clienteService: ClienteService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.cargarCliente()
  }

  cargarCliente(): void {

    this.activatedRoute.params.subscribe(
      params => {
        let id = params['id']
        if (id) {
          this.titulo = "Editar cliente"
          this.clienteService.getClienteById(id).subscribe(
            (cliente) => {
              return this.cliente = cliente;
            }
          )
        } else {
          this.titulo = "Crear cliente"
        }
      }

    )
  }

  public create(): void {
    console.log("clicked!")
    console.log(this.cliente)

    this.clienteService.create(this.cliente).subscribe(
      cliente => {
        this.router.navigate(['/clientes'])
        swal('Nuevo cliente', `cliente ${cliente.nombre} creado conéxito`, 'success')
      }
    )
  }

  update(): void {

    this.clienteService.update(this.cliente).subscribe(
      cliente => {
        this.router.navigate(['/clientes'])
        swal('Cliente actualizado', `Cliente ${cliente.nombre} actualizado con éxito!`, 'success')
      }
    )
  }

}
