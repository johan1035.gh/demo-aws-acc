import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'clientes-app';
  curso = 'Full statck app con spring5 y angular 7';
  profesor = 'Andrés Guzmán';
}
