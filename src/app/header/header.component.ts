import { Component } from '@angular/core';

@Component({
    selector : 'app-header',
    templateUrl : './Header.Component.html',
    styleUrls : ['./header.component.css']
})
export class HeaderComponent {
    public title: any = {appName:'Full-stack-app'};
}